package model;

public class Rechteck {
	// attribute
	private int x;
	private int y;
	private int breite;
	private int hoehe;

	public Rechteck() {
		this.setX(0);
		this.setY(0);
		this.setBreite(0);
		this.setHoehe(0);

	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.x = x;
		this.y = y;
		this.breite = breite;
		this.hoehe = hoehe;
	}

	// Methoden
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}

	@Override
	public String toString() {
		return "Rechteck [X=" + x + ", Y=" + y + ", hoehe=" + hoehe + ", breite=" + breite + "]";
	}
}
