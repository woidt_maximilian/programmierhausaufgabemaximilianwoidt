package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {
	public static Rechteck rechteck0 = new Rechteck(10, 10, 30, 40);
	public static Rechteck rechteck1 = new Rechteck(25, 25, 100, 20);
	public static Rechteck rechteck2 = new Rechteck(260, 10, 200, 100);
	public static Rechteck rechteck3 = new Rechteck(5, 500, 300, 25);
	public static Rechteck rechteck4 = new Rechteck(100, 100, 100, 100);

	public static Rechteck rechteck5 = new Rechteck();
	public static Rechteck rechteck6 = new Rechteck();
	public static Rechteck rechteck7 = new Rechteck();
	public static Rechteck rechteck8 = new Rechteck();
	public static Rechteck rechteck9 = new Rechteck();

	public static void main(String[] args) {
	
		rechteck5.setX(200);
		rechteck5.setY(200);
		rechteck5.setBreite(200);
		rechteck5.setHoehe(200);

		rechteck6.setX(800);
		rechteck6.setY(400);
		rechteck6.setBreite(20);
		rechteck6.setHoehe(20);

		rechteck7.setX(800);
		rechteck7.setY(450);
		rechteck7.setBreite(20);
		rechteck7.setHoehe(20);

		rechteck8.setX(850);
		rechteck8.setY(400);
		rechteck8.setBreite(20);
		rechteck8.setHoehe(20);

		rechteck9.setX(855);
		rechteck9.setY(455);
		rechteck9.setBreite(25);
		rechteck9.setHoehe(25);
		
		System.out.println("Rechteck 0 " + "Position X: " + rechteck0.getX() + "Position Y: " + rechteck0.getY() + "H�he: " + rechteck0.getHoehe() + "Breite: " + rechteck0.getBreite());
		System.out.println("Rechteck 1 " + "Position X: " + rechteck1.getX() + "Position Y: " + rechteck1.getY() + "H�he: " + rechteck1.getHoehe() + "Breite: " + rechteck1.getBreite());
		System.out.println("Rechteck 2 " + "Position X: " + rechteck2.getX() + "Position Y: " + rechteck2.getY() + "H�he: " + rechteck2.getHoehe() + "Breite: " + rechteck2.getBreite());
		System.out.println("Rechteck 3 " + "Position X: " + rechteck3.getX() + "Position Y: " + rechteck3.getY() + "H�he: " + rechteck3.getHoehe() + "Breite: " + rechteck3.getBreite());
		System.out.println("Rechteck 4 " + "Position X: " + rechteck4.getX() + "Position Y: " + rechteck4.getY() + "H�he: " + rechteck4.getHoehe() + "Breite: " + rechteck4.getBreite());
		System.out.println("Rechteck 5 " + "Position X: " + rechteck5.getX() + "Position Y: " + rechteck5.getY() + "H�he: " + rechteck5.getHoehe() + "Breite: " + rechteck5.getBreite());
		System.out.println("Rechteck 6 " + "Position X: " + rechteck6.getX() + "Position Y: " + rechteck6.getY() + "H�he: " + rechteck6.getHoehe() + "Breite: " + rechteck6.getBreite());
		System.out.println("Rechteck 7 " + "Position X: " + rechteck7.getX() + "Position Y: " + rechteck7.getY() + "H�he: " + rechteck7.getHoehe() + "Breite: " + rechteck7.getBreite());
		System.out.println("Rechteck 8 " + "Position X: " + rechteck8.getX() + "Position Y: " + rechteck8.getY() + "H�he: " + rechteck8.getHoehe() + "Breite: " + rechteck8.getBreite());
		System.out.println("Rechteck 9 " + "Position X: " + rechteck9.getX() + "Position Y: " + rechteck9.getY() + "H�he: " + rechteck9.getHoehe() + "Breite: " + rechteck9.getBreite());
		
		System.out.println(rechteck0.toString().equals("Rechteck 0: [x=10, y=10, breite=30, hoehe=40]"));
		BunteRechteckeController controller = new BunteRechteckeController();
        controller.add(rechteck0);
        controller.add(rechteck1);
        controller.add(rechteck2);
        controller.add(rechteck3);
        controller.add(rechteck4);
        controller.add(rechteck5);
        controller.add(rechteck6);
        controller.add(rechteck7);
        controller.add(rechteck8);
        controller.add(rechteck9);

        System.out.println(controller.toString());
	}
}
